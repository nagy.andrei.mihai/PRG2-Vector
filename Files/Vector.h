#ifndef Vector_H
#define Vector_H

#include<iostream>
#include<stdexcept>

using std::ostream;
using std::initializer_list;
using std::runtime_error;

template<typename T>
class Vector
{
public:
    //  TYPDEKLARATIONEN (ALIASE)
    using value_type = T;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using reference = value_type &;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;

private:
    Vector<T>::size_type max_sz; // max size
    Vector<T>::size_type sz; // actual size
    Vector<T>::pointer values;

public:
    class ConstIterator
    {
        Vector<T>::pointer const_ptr;
        const Vector<T> *vector;
        mutable Vector<T>::size_type counter = 0;
    public:
        Vector<T>::size_type count() const
        {
            return counter;
        }

        ConstIterator(Vector<T>::pointer ptr_param, const Vector<T> *vector_param) : const_ptr{ptr_param},
                                                                                     vector{vector_param}
        {
        }

        Vector<T>::const_reference operator*() const
        {
            if (const_ptr >= vector->values + vector->sz) throw std::runtime_error("ERROR operator*() - Out of bounds");
            counter++;
            return *const_ptr;
        }

        Vector<T>::const_pointer operator->() const
        {
            counter++;
            return const_ptr;
        }

        ConstIterator &operator++()
        {
            //if (++ptr > vector.values + vector.sz) throw std::runtime_error("ERROR operator++() - Out of bounds");
            ++const_ptr;
            return *this;
        }

        ConstIterator operator++(int)
        {
            ConstIterator result(*this);   // make a copy for result
            ++(*this);                // Now use the prefix version (see &operator++() above) to do the work
            return result;            // return the copy (the old) value.
        }

        ConstIterator &operator--()
        {
            //if (--ptr < vector.values) throw std::runtime_error("ERROR operator--() - Out of bounds");
            --const_ptr;
            return *this;
        }

        ConstIterator operator--(int)
        {
            ConstIterator result(*this);   // make a copy for result
            --(*this);                // Now use the prefix version (see &operator++() above) to do the work
            return result;            // return the copy (the old) value.
        }

        ConstIterator operator+(Vector<T>::size_type right_op) const
        {
            /*Iterator result_iterator(ptr, vector);
            if (ptr + right_op > vector.values + vector.sz)
                throw std::runtime_error("ERROR operator+(size_t) - Out of bounds");
            result_iterator.ptr += right_op;
            return result_iterator;*/

            return Iterator(const_ptr + right_op, vector);
        }

        ConstIterator operator-(Vector<T>::size_type right_op) const
        {
            /*Iterator result_iterator(ptr, vector);
            if (ptr - right_op < vector.values)
                throw std::runtime_error("ERROR operator-(size_t) - Out of bounds");
            result_iterator.ptr -= right_op;
            return result_iterator;*/

            return Iterator(const_ptr - right_op, vector);
        }

        inline friend bool operator!=(const ConstIterator &, const ConstIterator &);

        inline friend bool operator==(const ConstIterator &, const ConstIterator &);

        inline friend Vector<T>::difference_type operator-(const ConstIterator &, const ConstIterator &);

        inline friend bool operator<(const ConstIterator &, const ConstIterator &);

        inline friend bool operator>(const ConstIterator &, const ConstIterator &);

        inline friend bool operator<=(const ConstIterator &, const ConstIterator &);

        inline friend bool operator>=(const ConstIterator &, const ConstIterator &);
    };

    class Iterator
    {
        //automatisch friend von Vector
        Vector<T>::pointer ptr;
        const Vector<T> *vector;
        mutable Vector<T>::size_type counter = 0;
    public:
        Vector<T>::size_type count() const
        {
            throw runtime_error("Count method");
        }

        using value_type = Vector<T>::value_type;
        using difference_type = Vector<T>::difference_type;
        using reference = Vector<T>::reference;
        using pointer = Vector<T>::pointer;
        using iterator_category = std::forward_iterator_tag;

        Iterator(Vector<T>::pointer ptr_param, const Vector<T> *vector_param) : ptr{ptr_param},
                                                                                vector{vector_param}
        {
        }

        operator ConstIterator() const
        {
            return ConstIterator(ptr, vector);
        }

        Iterator &operator++()
        {
            //if (++ptr > vector.values + vector.sz) throw std::runtime_error("ERROR operator++() - Out of bounds");
            ++ptr;
            return *this;
        }

        Iterator operator++(int)
        {
            Iterator result(*this);   // make a copy for result
            ++(*this);                // Now use the prefix version (see &operator++() above) to do the work
            return result;            // return the copy (the old) value.
        }

        Iterator &operator--()
        {
            //if (--ptr < vector.values) throw std::runtime_error("ERROR operator--() - Out of bounds");
            --ptr;
            return *this;
        }

        Iterator operator--(int)
        {
            Iterator result(*this);   // make a copy for result
            --(*this);                // Now use the prefix version (see &operator++() above) to do the work
            return result;            // return the copy (the old) value.
        }

        Iterator operator+(Vector<T>::size_type right_op) const
        {
            /*Iterator result_iterator(ptr, vector);
            if (ptr + right_op > vector.values + vector.sz)
                throw std::runtime_error("ERROR operator+(size_t) - Out of bounds");
            result_iterator.ptr += right_op;
            return result_iterator;*/

            return Iterator(ptr + right_op, vector);
        }

        Iterator operator-(Vector<T>::size_type right_op) const
        {
            /*Iterator result_iterator(ptr, vector);
            if (ptr - right_op < vector.values)
                throw std::runtime_error("ERROR operator-(size_t) - Out of bounds");
            result_iterator.ptr -= right_op;
            return result_iterator;*/

            return Iterator(ptr - right_op, vector);
        }

        Iterator &operator=(const Iterator &right_op)
        {
            ptr = right_op.ptr;
            vector = right_op.vector;
            return *this;
        }

        Vector<T>::reference operator*()
        {
            if (ptr >= vector->values + vector->sz) throw std::runtime_error("ERROR operator*() - Out of bounds");
            return *ptr;
        }

        Vector<T>::pointer operator->()
        {
            return ptr;
        }
    };

    //  TYPDEKLARATIONEN (ALIASE) - cont.
    using iterator = Iterator;
    using const_iterator = ConstIterator;

    //  KONSTRUKTOREN
    Vector<T>() : max_sz{0}, sz{0}, values{nullptr}
    {}

    Vector<T>(Vector<T>::size_type s) : max_sz{s}, sz{0}
    {
        values = max_sz ? new T[max_sz]: nullptr;
    }

    Vector<T>(initializer_list<Vector<T>::value_type> li)
    {
        max_sz = li.size();
        sz = 0;
        values = max_sz ? new T[max_sz]: nullptr;
        if (values)
        {
            for (Vector<T>::const_reference elem : li)
                values[sz++] = elem;
        }

    }

    Vector<T>(const Vector<T> &original)
    {
        max_sz = original.max_sz;
        sz = original.sz;
        values = max_sz ? new T[max_sz] : nullptr;
        for (Vector<T>::size_type i = 0; i < sz; i++)
        {
            values[i] = original.values[i];
        }
    }

    //  DESTRUKTOR
    ~Vector<T>()
    {
        delete[] values;
    }

    //  METHODEN
    Vector<T>::size_type size() const
    {
        return sz;
    }

    Vector<T>::size_type max_size() const
    {
        return max_sz;
    }

    bool empty() const
    {
        return !sz;
    }

    void clear()
    {
        sz = 0;
        shrink_to_fit();
    }

    void reserve(Vector<T>::size_type newSize)
    {
        if (newSize <= max_sz) throw runtime_error("ERROR reserve - Neue Länge nicht groß genug");
        if (newSize < sz) throw runtime_error("ERROR reserve - Buffer to small");
        Vector<T>::pointer newVector{new T[newSize]};
        for (Vector<T>::size_type i{0}; i < max_sz; i++)
            newVector[i] = values[i];
        delete[] values;
        values = newVector;
        max_sz = newSize;
    }

    void shrink_to_fit()
    {
        Vector<T>::pointer newVector;
        newVector = sz ? new T[sz] : nullptr;
        for (Vector<T>::size_type i{0}; i < sz; i++)
            newVector[i] = values[i];
        delete[] values;
        values = newVector;
        max_sz = sz;
    }

    void push_back(Vector<T>::value_type new_element)
    {
        if (sz == max_sz) reserve(max_sz * 2 + 10);
        values[sz++] = new_element;
    }

    void pop_back()
    {
        if (!sz) throw runtime_error("ERROR pop_back - Keine Elemente im Vektor");
        else sz--;
    }

    //  ÜBERLADUNG VON OPERATOREN
    Vector<T>::reference operator[](Vector<T>::size_type index)
    {
        if (index >= sz) throw runtime_error("ERROR operator[] - Index out of bounds");
        else return values[index];
    }

    Vector<T> &operator=(const Vector<T> &right_op)
    {
        if (&right_op == this) return *this;

        if (values) delete[] values;

        max_sz = right_op.max_sz;
        sz = right_op.sz;
        values = max_sz ? new T[max_sz] : nullptr;
        for (Vector<T>::size_type i{0}; i < sz; i++)values[i] = right_op.values[i];
        return *this;
    }

    ostream &print(ostream &os) const
    {
        os << '[';
        for (Vector<T>::size_type i{0}; i < sz; i++)
        {
            if (i == 0) os << values[i];
            else os << ", " << values[i];
        }
        os << ']';
        return os;
    }

    //  ITERATOR METHODEN
    Vector<T>::iterator begin()
    {
        return Iterator(values, this);
    }

    Vector<T>::iterator end()
    {
        return Iterator(values + sz, this);
    }

    Vector<T>::const_iterator begin() const
    {
        return ConstIterator(values, this);
    }

    Vector<T>::const_iterator end() const
    {
        return ConstIterator(values + sz, this);
    }

    Vector<T>::iterator erase(Vector<T>::const_iterator pos)
    {
        auto diff = pos - begin();
        if (diff < 0 || static_cast<size_type>(diff) >= sz)
            throw runtime_error("ERROR erase() - Iterator out of bounds");
        size_type current{static_cast<size_type>(diff)};
        for (; current < sz - 1; ++current)
            values[current] = values[current + 1];
        --sz;
        return Iterator{values + current, this};
    }

    Vector<T>::iterator insert(Vector<T>::const_iterator pos, Vector<T>::const_reference val)
    {
        auto diff = pos - begin();
        if (diff < 0 || static_cast<size_type>(diff) > sz)
            throw runtime_error("ERROR insert() - Iterator out of bounds");
        size_type current{static_cast<size_type>(diff)};
        if (sz >= max_sz)
            reserve(max_sz * 2 + 10); //max_sz*2+10, wenn Ihr Container max_sz==0 erlaubt
        for (size_t i{sz}; i-- > current;)
            values[i + 1] = values[i];
        values[current] = val;
        ++sz;
        return Iterator{values + current, this};
    }

    // FRIEND METHODS -> Can be used by both Iterator and ConstIterator
    inline friend bool operator!=(const ConstIterator &left_op, const ConstIterator &right_op)
    {
        return left_op.const_ptr != right_op.const_ptr;
    }

    inline friend bool operator==(const ConstIterator &left_op, const Vector<T>::ConstIterator &right_op)
    {
        return !(left_op != right_op);
    }

    inline friend Vector<T>::difference_type
    operator-(const ConstIterator &left_op, const ConstIterator &right_op)
    {
        return left_op.const_ptr - right_op.const_ptr;
    }

    inline friend bool operator<(const ConstIterator &left_op, const ConstIterator &right_op)
    {
        return left_op.const_ptr < right_op.const_ptr;
    }

    inline friend bool operator>(const ConstIterator &left_op, const ConstIterator &right_op)
    {
        return left_op.const_ptr > right_op.const_ptr;
    }

    inline friend bool operator<=(const ConstIterator &left_op, const ConstIterator &right_op)
    {
        return left_op.const_ptr <= right_op.const_ptr;
    }

    inline friend bool operator>=(const ConstIterator &left_op, const ConstIterator &right_op)
    {
        return left_op.const_ptr >= right_op.const_ptr;
    }
};

// --------------------------------------------------------------------------------------------------------------------
// VECTOR METHODS

//Globale Funktionen - Ueberladungen:
//Ueberladungen entweder als friend oder ausserhalb der klasse
template<typename T>
ostream &operator<<(ostream &os, const Vector<T> &vector)
{
    return vector.print(os);
}

#endif // Vector_H
